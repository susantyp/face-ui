import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);
const routes = [
  { path: '/', component: () => import('@/views/login/index') },
  {
    path: '/home',
    component: () => import('@/views/home/index'),
    redirect: '/desktop',
    children: [
      { path: '/desktop', component: () => import('@/components/desktop/index') },
      { path: '/faceList', component: () => import('@/components/face/index') },
      { path: '/faceLog', component: () => import('@/components/faceLog/index') }
    ]
  }
];
const router = new VueRouter({ routes });
router.beforeEach((to, from, next) => (to.path === '/' || localStorage.getItem('face_token')) ? next() : next('/'));
export default router;
