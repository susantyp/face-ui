import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import axios from "@/axios";

// 引入框架
import 'face-effet/effet/effet.css'
import faceEffet from 'face-effet/effet/effet.js'
// 注册为全局
Vue.prototype.$faceEffet = faceEffet

// 官网地址：https://faceeffet.com/

/**
 * 引入最新人脸动作，增加眨眨眼，左右摇头，张张嘴动作
 */

Vue.config.productionTip = false
Vue.prototype.$http = axios

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
